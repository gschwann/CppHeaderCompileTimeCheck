//////////////////////////////////////////////////
// boost vs. std

//#include <boost/thread/mutex.hpp>
//#include <mutex>

//#include <boost/thread/shared_mutex.hpp>
//#include <shared_mutex>

//#include <boost/thread.hpp>
//#include <boost/thread/thread.hpp>
//#include <thread>

//#include <boost/function.hpp>
//#include <functional>

//////////////////////////////////////////////////
// boost vs. Qt

//#include <boost/test/unit_test.hpp>
//#include <QtTest/QTest>

//#include <boost/log/core.hpp>
//#include <boost/log/sources/record_ostream.hpp>
//#include <boost/log/sources/severity_channel_logger.hpp>
//#include <boost/log/sources/global_logger_storage.hpp>
//#include <boost/log/sources/severity_feature.hpp>
//#include <boost/log/sources/channel_logger.hpp>
//#include <QDebug>

//////////////////////////////////////////////////
// boost vs. std vs Qt

//#include <memory>
//#include <boost/shared_ptr.hpp>
//#include <QSharedPointer>

//////////////////////////////////////////////////
// std impact

//#include <vector>
//#include <map>

//////////////////////////////////////////////////
// boost impact

//#include <iostream>
//#include <boost/geometry/core/cs.hpp>
//#include <boost/geometry/geometries/point.hpp>
//#include <boost/geometry/geometries/register/linestring.hpp>
//#include <boost/geometry/geometries/register/point.hpp>
//#include <boost/geometry/algorithms/distance.hpp>
//#include <boost/geometry/arithmetic/arithmetic.hpp>

//#include <boost/bimap.hpp>

//#include <boost/asio/io_service.hpp>
//#include <boost/asio.hpp>

//#include <boost/variant.hpp>
//#include <boost/variant/variant.hpp>
//#include <boost/variant/static_visitor.hpp>
//#include <boost/version.hpp>
//#include <boost/rational.hpp>

//#include <boost/algorithm/string/predicate.hpp>

//////////////////////////////////////////////////
// std vs Qt

//#include <vector>
//#include <QVector>

//////////////////////////////////////////////////
// Qt impact

//#include <QObject>

//////////////////////////////////////////////////
// Test frameworks impact
#include <QtTest/QTest>
#include <boost/test/unit_test.hpp>
#include <catch.hpp>
#include <gtest/gtest.h>
