TEMPLATE = app
QT += core testlib
CONFIG += console test c++14
CONFIG -= app_bundle

#CONFIG += precompile_header
#PRECOMPILED_HEADER = pch.h

macx {
    INCLUDEPATH += /usr/local/include
}

HEADERS += \
    content.h \
#    pch.h

SOURCES = \
    main.cpp \
    src/file001.cpp \
    src/file002.cpp \
    src/file003.cpp \
    src/file004.cpp \
    src/file005.cpp \
    src/file006.cpp \
    src/file007.cpp \
    src/file008.cpp \
    src/file009.cpp \
    src/file010.cpp \

LIBS += \
    -l boost_unit_test_framework \
