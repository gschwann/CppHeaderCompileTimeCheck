#ifndef PCH_H
#define PCH_H

#if defined __cplusplus
// Add C++ includes here

// std headers
#include <functional>
#include <map>
#include <memory>
#include <mutex>
#include <thread>
#include <vector>

// boost headers
#include <boost/algorithm/string/predicate.hpp>
#include <boost/asio.hpp>
#include <boost/bimap.hpp>
#include <boost/function.hpp>
#include <boost/geometry.hpp>
#include <boost/log/core.hpp>
#include <boost/log/sources/record_ostream.hpp>
#include <boost/log/sources/severity_channel_logger.hpp>
#include <boost/log/sources/global_logger_storage.hpp>
#include <boost/log/sources/severity_feature.hpp>
#include <boost/log/sources/channel_logger.hpp>
#include <boost/rational.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/test/unit_test.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/shared_mutex.hpp>
#include <boost/thread.hpp>
#include <boost/version.hpp>
#include <boost/variant.hpp>

// Qt headers
#include <QDebug>
#include <QObject>
#include <QSharedPointer>
#include <QVector>
#include <QtTest/QTest>

#endif // cpp includes

#endif // PCH_H
